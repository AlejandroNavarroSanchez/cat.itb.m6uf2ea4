import Model.DepartamentosEntity;
import Model.EmpleadosEntity;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;

import javax.persistence.metamodel.EntityType;

import java.util.List;
import java.util.Map;

public class Main {
    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public static void main(final String[] args) throws Exception {
//        final Session session = getSession();
//        try {
//            System.out.println("querying all the managed entities...");
//            final Metamodel metamodel = session.getSessionFactory().getMetamodel();
//            for (EntityType<?> entityType : metamodel.getEntities()) {
//                final String entityName = entityType.getName();
//                final Query query = session.createQuery("from " + entityName);
//                System.out.println("executing: " + query.getQueryString());
//                for (Object o : query.list()) {
//                    System.out.println("  " + o);
//                }
//            }
//        } finally {
//            session.close();
//        }

//        ex1();
//        ex2();
//        ex3();
//        ex4();
//        ex5();
//        ex6();
    }

    /**
     * Exercici 1. Inserta aquests dos nous departaments a la taula departamentos
     * dept_no dnombre loc
     * 60 TECNOLOGIA BARCELONA
     * 70 INFORMATICA SEVILLA
     */
    private static void ex1() {
        // Get session
        Session session = getSession();

        // Begin transaction
        Transaction t = session.beginTransaction();

        //Create DepartamentosEntity Model data
        DepartamentosEntity dep1 = new DepartamentosEntity();
        dep1.setDeptNo((short) 60);
        dep1.setDnombre("TECNOLOGIA");
        dep1.setLoc("BARCELONA");

        DepartamentosEntity dep2 = new DepartamentosEntity();
        dep2.setDeptNo((short) 70);
        dep2.setDnombre("INFORMATICA");
        dep2.setLoc("SEVILLA");

        session.save(dep1);
        session.save(dep2);

        // Commit the transaction and close the session
        t.commit();

        session.close();
        System.out.println("successfully persisted DepartamentosEntity details");
    }

    /**
     * Exercici 2. Inserta un nou empleat a cada departament nou, tria tu les dades dels nous empleats.
     */
    private static void ex2() {
        // Get session
        Session session = getSession();

        // Begin transaction
        Transaction t = session.beginTransaction();

        //Create EmpleadosEntity Model data
        EmpleadosEntity emp1 = new EmpleadosEntity();
        emp1.setEmpNo((short) 8000);
        emp1.setApellido("GARCIA");
        emp1.setOficio("CAJERO");
        emp1.setDir((short) 8000);
        // fecha de alta
        java.util.Date hoy = new java.util.Date();
        java.sql.Date fecha = new java.sql.Date(hoy.getTime());
        emp1.setFechaAlt(fecha);
        emp1.setSalario(1600.0);
        emp1.setComision(70.0);
        //carreguem el Departament 10
        DepartamentosEntity dep1 = session.load(DepartamentosEntity.class, (short) 60);
        emp1.setDepartamentosByDeptNo(dep1);

        EmpleadosEntity emp2 = new EmpleadosEntity();
        emp2.setEmpNo((short) 8001);
        emp2.setApellido("GARCIA");
        emp2.setOficio("CAJERO");
        emp2.setDir((short) 8001);
        emp2.setFechaAlt(fecha);
        emp2.setSalario(1700.0);
        emp2.setComision(80.0);
        //carreguem el Departament 10
        DepartamentosEntity dep2 = session.load(DepartamentosEntity.class, (short) 70);
        emp2.setDepartamentosByDeptNo(dep2);

        session.save(emp1);
        session.save(emp2);

        // Commit the transaction and close the session
        t.commit();

        session.close();
        System.out.println("successfully persisted EmpleadosEntity details");
    }

    /**
     * Exercici 3. Actualitza el nom del departament número 20, ara es dirà RECERCA.
     */
    private static void ex3() {
        // Get session
        Session session = getSession();
        Query query = session.createQuery("update DepartamentosEntity set dnombre=:dnombre where deptNo=:deptNo");
        query.setParameter("dnombre", "RECERCA");
        query.setParameter("deptNo", (short) 20);
        // Begin transaction
        Transaction t = session.beginTransaction();
        int result = query.executeUpdate();
        // Commit the transaction and close the session
        t.commit();
        System.out.println("No of rows updated: "+result);

        session.close();
    }

    /**
     * Exercici 4. Actualitza el salari de l’empleat amb codi 7499, ara cobra 2100.
     */
    private static void ex4() {
        // Get session
        Session session = getSession();
        Query query = session.createQuery("update EmpleadosEntity set salario=:salario where empNo=:empNo");
        query.setParameter("salario", 2100.0);
        query.setParameter("empNo", (short) 7499);
        // Begin transaction
        Transaction t = session.beginTransaction();
        int result = query.executeUpdate();
        // Commit the transaction and close the session
        t.commit();
        System.out.println("No of rows updated: "+result);

        session.close();
    }

    /**
     * Excercici 5. Elimina l’empleat que es diu SALA.
     */
    private static void ex5() {
        // Get session
        Session session = getSession();
        Query query = session.createQuery("delete from EmpleadosEntity where apellido=:apellido");
        query.setParameter("apellido", "SALA");
        // Begin transaction
        Transaction t = session.beginTransaction();
        int result = query.executeUpdate();
        // Commit the transaction and close the session
        t.commit();
        System.out.println("No of rows Deleted: "+result);

        session.close();
    }

    /**
     * Excercici 6. Fes una consulta per mostrar els empleats que cobrin més de 2000.
     */
    private static void ex6() {
        // Get session from Sesson factory
        Session session = getSession();
        @SuppressWarnings("rawtypes")
        Query query = session.createQuery("from EmpleadosEntity");
        List<EmpleadosEntity> list = query.getResultList();
        int count = 0;
        for (EmpleadosEntity emp : list) {

            if (emp.getSalario() > 2000.0) {
                System.out.println(emp);
                count++;
            }
        }
        System.out.println("Number of EmpleadosEntity present--> "+count);

        session.close();
    }
}